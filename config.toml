# Page settings
baseurl = "/"
languageCode = "en-us"
title = "Juraci Paixão Kröhling"
theme = "kroehling-hugo-orbit-theme"

# Do not build files for websites. Need them? Set to false
disableRSS = true
disableSitemap = true
disable404 = true

[params]

# Theme styles

    # The original template comes with 6 colour schemes. You may choose styles below.
    # "styles.css", "styles-2.css", "styles-3.css", "styles-4.css", "styles-5.css", "styles-6.css"
    # are available. Also the source LESS files are included so
    # it’s quick and easy to change the styling and colour scheme.
    styles = "styles-2.css"

# Sidebar sections

    # Profile section
    [params.profile]
        name = "Juraci Paixão Kröhling"
        tagline = "Software Engineer"
        avatar = "avatar.jpg"

    # Contact section
    [params.contact]
        enable = true

        [[params.contact.list]]
        class = "email"
        icon = "fa-envelope"
        url = "mailto: juraci@kroehling.de"
        title = "juraci@kroehling.de"

        [[params.contact.list]]
        class = "website"
        icon = "fa-globe"
        url = "//kroehling.de"
        title = "kroehling.de"

        [[params.contact.list]]
        class = "github"
        icon = "fa-github"
        url = "//github.com/jpkrohling"
        title = "github.com/jpkrohling"

        [[params.contact.list]]
        class = "twitter"
        icon = "fa-twitter"
        url = "//twitter.com/jpkrohling"
        title = "@jpkrohling"

        [[params.contact.list]]
        class = "speakerhub"
        icon = "fa-microphone"
        url = "//speakerhub.com/speaker/juraci-paixao-krohling"
        title = "juraci-paixao-krohling"

    # Education section
    [params.education]
        enable = true
        title = "Education"

        [[params.education.list]]
        degree = "High School"
        college = "Colégio Salesiano São Gonçalo, Cuiabá, Mato Grosso"
        dates = "2000"

    # Languages section
    [params.language]
        enable = true
        title = "Languages"

        [[params.language.list]]
        language = "Portuguese"
        level = "Native"

        [[params.language.list]]
        language = "English"
        level = "C1, self-assessed"

        [[params.language.list]]
        language = "German"
        level = "B1, certified"

    # Interests section
    [params.interests]
        enable = true
        title = "Interests"

        [[params.interests.list]]
        interest = "Java, Golang"

        [[params.interests.list]]
        interest = "Kubernetes, OpenShift"

        [[params.interests.list]]
        interest = "Microservices, Cloud Native"

# Main body sections

    # You may use markdown in summary, details and intro fields. But don't overdose, it's resume!:)

    # Summary section
    [params.summary]
        enable = true
        icon = "fa-user"
        title = "Career Profile"
        summary = """
I'm a software engineer with experience in development and architecture of modern, cloud-native applications, incorporating secure coding and quality assurance practices. My primary background is backend Java and Go development, and my current interests are around Microservices and Cloud Native applications, including Kubernetes and OpenShift. For the past few years, I've been developing solutions in the area of Observability, contributing to projects like [Jaeger](https://www.jaegertracing.io/) and [OpenTelemetry](https://opentelemetry.io/).

Besides coding, I enjoy mentoring others, writing blog posts, and giving talks about areas I actively work on.
        """

    # Experiences section
    [params.experiences]
        enable = true
        icon = "fa-briefcase"
        title = "Experiences"

        [[params.jobs.list]]
        position = "Principal Software Engineer, Tech Lead"
        dates = "Jan 2021 - Present"
        company = "Red Hat GmbH, Berlin, Germany"
        details = """Tech lead for the distributed tracing team, setting the engineering direction of what is made available as part of Red Hat products, including Red Hat OpenShift distributed tracing, Red Hat OpenShift Service Mesh, among others. Part of my duties is to understand what Red Hat customers expect from our solutions and work with the open source communities to address those needs, upstream first. This results in new sub-projects, such as the [Jaeger Operator](https://github.com/jaegertracing/jaeger-operator), [OpenTelemetry Operator](https://github.com/open-telemetry/opentelemetry-operator/), and [OpenTelemetry Collector Builder](https://github.com/open-telemetry/opentelemetry-collector-builder/), as well as specific features for Jaeger and OpenTelemetry Collector, especially around areas like [resiliency](https://github.com/open-telemetry/opentelemetry-collector-contrib/tree/v0.29.0/exporter/loadbalancingexporter), [security](https://github.com/open-telemetry/opentelemetry-collector/tree/v0.29.0/config/configauth) and [multi-tenancy](https://github.com/open-telemetry/opentelemetry-collector-contrib/tree/v0.29.0/processor/routingprocessor).

I'm a maintainer in the [Jaeger](https://jaeger.devstats.cncf.io/d/9/developer-activity-counts-by-repository-group-table?orgId=1&var-period_name=Last%20year&var-metric=contributions&var-repogroup_name=All&var-country_name=All) project and approver for the [OpenTelemetry Collector](https://opentelemetry.devstats.cncf.io/d/9/developer-activity-counts-by-repository-group-table?orgId=1&var-period_name=Last%20year&var-metric=contributions&var-repogroup_name=open-telemetry%2Fopentelemetry-collector&var-country_name=All), being consistently among the top developers in terms of project contributions. 

Since 2017, I'm a regular mentor of interns as part of the Outreachy program, with sponsorship from the CNCF - Cloud Native Computing Foundation.

I was part of the program committee and/or track co-chair for different conferences, including [KubeCon China 2020](https://www.credly.com/badges/3d57a253-a6d6-4bab-89fa-bd0ddc95f117/public_url), [KubeCon EU 2021](https://www.credly.com/badges/93c23d83-b218-4239-bc4e-46427af7fb95/public_url) and KubeCon NA 2021, and "The Dev Conf - Connections International (Brazil)" for the "APIs and Microservices" track.

Keywords: Golang, Kubernetes, OpenShift, Cloud Native, OpenTelemetry, Observability, Jaeger
"""

        [[params.jobs.list]]
        position = "Senior Software Engineer"
        dates = "Nov 2013 - Dec 2020"
        company = "Red Hat GmbH, Munich and Berlin, Germany"
        details = """Developer on the Kiali team, focusing on the distributed tracing components. Active in the OpenTracing and OpenTelemetry communities and maintainer on the Jaeger project, having held conference talks and meetups on the subject.

Previously, worked as sole developer on the Hawkular Accounts module, where I had the opportunity to also contribute to the Keycloak project.

I was also one of the sustaining engineers for the JBoss Enterprise Portal Platform, as well as core developer on GateIn, being the lead for the WSRP module.

Keywords: Golang, Java, Hawkular, Docker, Kubernetes, OpenShift, Microservices, Cloud Native, OpenTracing, Jaeger
"""

        [[params.jobs.list]]
        position = "Senior Quality Assurance Analyst"
        dates = "Oct 2012 - Oct 2013"
        company = "Paymill GmbH, Munich, Germany"

        [[params.jobs.list]]
        position = "Lead QA and Test Engineer"
        dates = "Jun 2011 - Sep 2012"
        company = "Motorola Mobility GmbH, Munich, Germany"

        [[params.jobs.list]]
        position = "JBoss QE Local Lead"
        dates = "Sep 2009 - May 2011"
        company = "Red Hat s.r.o., Brno, Czech Republic"

        [[params.jobs.list]]
        position = "QE Engineer"
        dates = "May 2008 - May 2011"
        company = "Red Hat s.r.o., Brno, Czech Republic"

        [[params.jobs.list]]
        position = "Senior Developer"
        dates = "Jul 2006 - Mar 2008"
        company = "Citigroup, São Paulo, Brazil"

        [[params.jobs.list]]
        position = "Owner"
        dates = "Oct 2005 - Sep 2006"
        company = "railsmate, São Paulo, Brazil"

        [[params.jobs.list]]
        position = "Web Developer"
        dates = "Oct 2005 - Apr 2006"
        company = "Insite Soluções Internet, São Paulo, Brazil"

        [[params.jobs.list]]
        position = "Java Developer"
        dates = "Mar 2005 - Oct 2005"
        company = "IBM, Hortolândia, Brazil"

        [[params.jobs.list]]
        position = "Further job history available upon request"
        dates = "Aug 2000 - Mar 2005"

    [params.patents]
        enable = true
        icon = "fa-lightbulb-o"
        title = "Patents"
        intro =  "As part of my daily job, I was fortunate to face problems that required innovative solutions. Some of them could be translated into patents, like the following."

        [[params.patents.list]]
        title = "Software tracing in a multitenant environment"
        url = "https://patents.google.com/patent/US20200233779A1"
        tagline = "Process to configure a span router to direct the payload to the appropriate backend based on metadata from the incoming connection, or metadata from the span itself."

        [[params.patents.list]]
        title = "Automatic microservice problem detection in enterprise applications"
        url = "https://patents.google.com/patent/US20180270122A1"
        tagline = "Process to build application profiles based on request-scoped path across distributed systems (such as microservices), so that two similar requests can be compared, allowing an external observer to detect anomalies"

        [[params.patents.list]]
        title = " Proxy with a function as a service (faas) support"
        url = "https://patents.google.com/patent/US20180375712A1"
        tagline = "Process to allow proxies to execute a set of functions as a service (FaaS) through a FaaS provider, in addition to proxying the request to an underlying upstream"

        [[params.patents.list]]
        title = "Multi-Tenant Enterprise Application Management"
        url = "https://patents.google.com/patent/US20170222997"
        tagline = "Process to allow existing enterprise applications to use current technologies to achieve multi tenancy"

        [[params.patents.list]]
        title = "Secret store for oauth offline tokens"
        url = "https://patents.google.com/patent/US20170214683A1"
        tagline = "Process to use an API key/secret store backed by OAuth Offline Tokens"

    [params.events]
        enable = true
        icon = "fa-microphone"
        title = "Events"
        intro =  """I enjoy attending conferences and talking about projects that I work on. I had the pleasure to be engaged, as a speaker or otherwise noted, at the following ones.

For a complete list of events, check my [SpeakerHub](https://speakerhub.com/speaker/juraci-paixao-krohling) account"""

        [[params.events.list]]
        event = "KubeCon + CloudNativeCon Europe 2021"
        type = "Conference"
        role = "Track co-chair and member of the Program Committee for the Observability track"
        date = "2021-05-04"
        location = "Virtual"
        url = "https://events.linuxfoundation.org/kubecon-cloudnativecon-europe/"

        [[params.events.list]]
        event = "Cloud Native + Open Source Virtual Summit APAC 2020"
        type = "Conference"
        role = "Member of the Program Committee for the Observability track"
        date = "2020-07-30"
        location = "Virtual"
        url = "https://cncf.lfasiallc.cn/"

        [[params.events.list]]
        event = "FOSDEM 2020"
        type = "Conference"
        title = "Distributed Tracing for beginners"
        role = "Speaker"
        date = "2020-02-01"
        location = "Brussels (Belgium)"
        url = "https://fosdem.org/2020/schedule/event/tracing_beginners/"

        [[params.events.list]]
        event = "Devoxx Belgium 2019"
        type = "Conference"
        title = "Observing chaos: how distributed tracing brings observability to a service mess"
        role = "Speaker"
        date = "2019-11-06"
        location = "Antwerp (Belgium)"
        url = "https://devoxx.be/talk/?id=19503"

        [[params.events.list]]
        event = "KubeCon + CloudNativeCon Europe 2019"
        type = "Conference"
        title = "Intro + Deep Dive: Jaeger"
        role = "Speaker"
        date = "2019-05-23"
        location = "Barcelona (Spain)"
        url = "https://sched.co/MPkv/"

        [[params.events.list]]
        event = "Red Hat Summit 2019"
        type = "Conference"
        title = "What are my microservices doing?"
        role = "Speaker"
        date = "2019-05-09"
        location = "Boston, MA (USA)"
        url = "https://summit.redhat.com/conference/sessions"

        [[params.events.list]]
        event = "Great Indian Developer Summit 2019"
        type = "Conference"
        title = "Advanced Distributed Tracing"
        role = "Speaker"
        date = "2019-04-25"
        location = "Bangalore (India)"
        url = "https://www.developermarch.com/developersummit/session.html?insert=JuraciPaixao1"

        [[params.events.list]]
        event = "Open Source Summit EU 2018"
        type = "Conference"
        title = "What are My Microservices Doing?"
        role = "Speaker"
        date = "2018-10-24"
        location = "Edinburgh (Scotland)"
        url = "https://sched.co/FxW3"

        [[params.events.list]]
        event = "JavaLand"
        type = "Conference"
        title = "Finding Performance Bottlenecks with Distributed Tracing"
        role = "Speaker"
        date = "2018-03-14"
        location = "Cologne (Germany)"
        url = "https://programm.javaland.eu/2018/#/scheduledEvent/549371"

        [[params.events.list]]
        event = "Open Source Summit NA"
        type = "Conference"
        title = "OpenTracing: One Instrumentation for Metrics, Logs and Distributed Tracing"
        role = "Co-speaker"
        date = "2017-09-11"
        location = "Los Angeles, CA (USA)"
        url = "http://sched.co/Boku"

    # Projects section
    [params.projects]
        enable = true
        icon = "fa-archive"
        title = "Projects"
        intro =  ""

        [[params.projects.list]]
        title = "Tech reviewer - Enterprise JavaBeans 3.1 (O'Reilly Media, Inc.)"
        url = "http://shop.oreilly.com/product/9780596158033.do"
        tagline = "ISBN: 9781449396961 - Credited as Technical Reviewer"

        [[params.projects.list]]
        title = "Tech reviewer - WildFly Cookbook (Packt Pub)"
        url = "https://www.packtpub.com/networking-and-servers/wildfly-cookbook"
        tagline = "ISBN: 9781784392413 - Credited as Technical Reviewer"

    # Articles & Blog posts section
    [params.publications]
        enable = true
        icon = "fa-book"
        title = "Articles & Blog posts"

        [[params.publications.list]]
        title = "Deploying the OpenTelemetry Collector on Kubernetes"
        url = "https://medium.com/opentelemetry/deploying-the-opentelemetry-collector-on-kubernetes-2256eca569c9"
        publication = "OpenTelemetry blog"
        date = "January 2021"

        [[params.publications.list]]
        title = "Securing your OpenTelemetry Collector"
        url = "https://medium.com/opentelemetry/securing-your-opentelemetry-collector-1a4f9fa5bd6f"
        publication = "OpenTelemetry blog"
        date = "November 2020"

        [[params.publications.list]]
        title = "Extending the OpenTelemetry Collector with your own components"
        url = "https://medium.com/opentelemetry/extending-the-opentelemetry-collector-with-your-own-components-64c10cf675db"
        publication = "OpenTelemetry blog"
        date = "October 2020"

        [[params.publications.list]]
        title = "Building your own OpenTelemetry Collector distribution"
        url = "https://medium.com/opentelemetry/building-your-own-opentelemetry-collector-distribution-42337e994b63"
        publication = "OpenTelemetry blog"
        date = "September 2020"

        [[params.publications.list]]
        title = "Build a monitoring infrastructure for your Jaeger installation"
        url = "https://developers.redhat.com/blog/2019/08/28/build-a-monitoring-infrastructure-for-your-jaeger-installation/"
        publication = "Red Hat Developer and [InfoQ China](https://www.infoq.cn/article/X9ZNas5kbRRBprSbCamD)"
        date = "August 2019"

        [[params.publications.list]]
        title = "A guide to the open source distributed tracing landscape"
        url = "https://developers.redhat.com/blog/2019/05/01/a-guide-to-the-open-source-distributed-tracing-landscape/"
        publication = "Red Hat Developer and [InfoQ China](https://www.infoq.cn/article/X9ZNas5kbRRBprSbCamD)"
        date = "May 2019"

        [[params.publications.list]]
        title = "Tuning Jaeger’s performance"
        url = "https://medium.com/jaegertracing/tuning-jaegers-performance-7a60864cf3b1"
        publication = "Jaeger Tracing blog"
        date = "March 2019"

        [[params.publications.list]]
        title = "Help! Something is wrong with my Jaeger installation!"
        url = "https://medium.com/jaegertracing/help-something-is-wrong-with-my-jaeger-installation-68874395a7a6"
        publication = "Jaeger Tracing blog"
        date = "January 2019"

        [[params.publications.list]]
        title = "Running Jaeger Agent on bare metal"
        url = "https://medium.com/jaegertracing/running-jaeger-agent-on-bare-metal-d1fc47d31fab"
        publication = "Jaeger Tracing blog and [DZone](https://dzone.com/articles/running-jaeger-agent-on-bare-metal)"
        date = "December 2018"

        [[params.publications.list]]
        title = "Distributed tracing in a microservices world"
        url = "https://opensource.com/article/18/9/distributed-tracing-microservices-world"
        publication = "opensource.com"
        date = "September 2018"

        [[params.publications.list]]
        title = "Jaeger Operator for Kubernetes"
        url = "https://medium.com/jaegertracing/jaeger-operator-for-kubernetes-b3128f3c4943"
        publication = "Jaeger Tracing blog"
        date = "September 2018"

        [[params.publications.list]]
        title = "Automatic tracing of Java EE applications with WildFly 14 and Jaeger"
        url = "https://medium.com/jaegertracing/automatic-tracing-of-java-ee-applications-with-wildfly-14-and-jaeger-176366927212"
        publication = "Jaeger Tracing blog and [DZone](https://dzone.com/articles/automatic-tracing-of-java-ee-applications-with-wil)"
        date = "September 2018"

        [[params.publications.list]]
        title = "The life of a span"
        url = "https://medium.com/jaegertracing/the-life-of-a-span-ee508410200b"
        publication = "Jaeger Tracing blog and [DZone](https://dzone.com/articles/the-life-of-a-span)"
        date = "July 2018"

        [[params.publications.list]]
        title = "Protecting Jaeger UI with an OAuth sidecar Proxy (revised)"
        url = "https://medium.com/jaegertracing/protecting-jaeger-ui-with-an-oauth-sidecar-proxy-34205cca4bb1"
        publication = "Jaeger Tracing blog"
        date = "April 2018"

        [[params.publications.list]]
        title = "Getting Jaeger’s Java Client internal metrics into Prometheus"
        url = "https://medium.com/jaegertracing/getting-jaegers-java-client-internal-metrics-into-prometheus-85b01afb9baa"
        publication = "Jaeger Tracing blog and [DZone](https://dzone.com/articles/getting-jaegers-java-client-internal-metrics-into)"
        date = "March 2018"

        [[params.publications.list]]
        title = "Protecting the collection of spans"
        url = "https://medium.com/jaegertracing/protecting-the-collection-of-spans-1948d88682e5"
        publication = "Jaeger Tracing blog and [DZone](https://dzone.com/articles/protecting-the-collection-of-spans)"
        date = "December 2017"

        [[params.publications.list]]
        title = "Jaeger and multitenancy"
        url = "https://medium.com/jaegertracing/jaeger-and-multitenancy-99dfa1d49dc0"
        publication = "Jaeger Tracing blog and [DZone](https://dzone.com/articles/jaeger-and-multitenancy)"
        date = "October 2017"

        [[params.publications.list]]
        title = "Deployment strategies for the Jaeger Agent"
        url = "https://medium.com/jaegertracing/deployment-strategies-for-the-jaeger-agent-1d6f91796d09"
        publication = "Jaeger Tracing blog"
        date = "October 2017"

        [[params.publications.list]]
        title = "OpenTracing EJB instrumentation"
        url = "https://www.hawkular.org/blog/2017/07/opentracing-ejb.html"
        publication = "Hawkular blog and [DZone](https://dzone.com/articles/opentracing-ejb-instrumentation-on-wildfly-swarm)"
        date = "July 2017"

        [[params.publications.list]]
        title = "Protecting Jaeger UI With a Sidecar Security Proxy"
        url = "https://www.hawkular.org/blog/2017/07/jaeger-with-security-proxy.html"
        publication = "Hawkular blog and [DZone](https://dzone.com/articles/protecting-jaeger-ui-with-a-sidecar-security-proxy)"
        date = "July 2017"

    # Skills section
    [params.skills]
        enable = true
        icon = "fa-rocket"
        title = "Skills & Proficiency"

        [[params.skills.list]]
        skill = "Golang"
        level = "98%"

        [[params.skills.list]]
        skill = "Java, Jakarta EE"
        level = "80%"

        [[params.skills.list]]
        skill = "Microservices"
        level = "95%"

        [[params.skills.list]]
        skill = "Distributed systems"
        level = "95%"

        [[params.skills.list]]
        skill = "RDMBS (PostgreSQL, MySQL, MS SQL Server)"
        level = "80%"

        [[params.skills.list]]
        skill = "Elasticsearch"
        level = "75%"

        [[params.skills.list]]
        skill = "NoSQL (Cassandra, MongoDB)"
        level = "50%"

        [[params.skills.list]]
        skill = "Front-end technologies"
        level = "20%"

    # Footer section

    # The original template is released under the Creative Commons Attribution 3.0 License.
    # Please keep the original attribution link when using for your own project.
    # If you'd like to use the template without the attribution,
    # you can check out other license options via template author's website: themes.3rdwavemedia.com
    #
    # As for Hugo port you may rewrite the "Ported for..." line with setting your name below.

    [params.footer]
        copyright = ""
